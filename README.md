# Fetching data from API
A simplistic example of combining a few popular packages to build a lightweight app for fetching data from third party API.

It includes the following out of the box:
- Routing via ["nikic/fast-route"](https://github.com/nikic/FastRoute) - see `app/routes.php`
- Dependency Injection via ["php-di/php-di"](https://github.com/PHP-DI/PHP-DI) - see `app/di.php`
- Template engine via ["twig/twig"](http://twig.sensiolabs.org/)
- API data is requested via Guzzle (see `.env` for where to put the API credentials and see `Views/` for all the templates)
- Error logging via [Monolog](https://github.com/Seldaek/monolog)
- Input validation with ["beberlei/assert"](https://github.com/beberlei/assert) (usage example in `Models/Question.php`)
- Basic database access via [PDO](http://php.net/manual/en/book.pdo.php) - connection defined in `.env` file.
- Redis for storing API results via ["predis/predis"](https://packagist.org/packages/predis/predis)

#### Prerequisites
* [composer](https://getcomposer.org)
* [redis](https://redis.io/topics/quickstart)

#### Steps
1. `git clone` this repository
2. `cd` into the cloned repository
3. `composer install`
5. `php -S localhost:port -t public/`
6. `redis-server` needs to be started through the terminal


From here, you should be able to access http://localhost:port/. 

You will also need to add some values to your `.env` for Database, API, etc.


I made a script to create the Database for the Employees. Open `data/employees.sql` to check what's defined and run the SQL script by either pasting it into a MySQL management tool, or directly from the command line:

```bash
mysql -u myuser -p < data/employees.sql

```

Anyways, because we're using Redis this is not needed at the moment.