<?php

/**
 * File is consumed when the app is being loaded.
 *
 * These definitions will be available in the app's controllers etc.
 */

use KKHiring\Controllers\EmployeesController;

return [
    'EmployeesController' => function() {
        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../src/KKHiring/Views');
        $controller = new EmployeesController(new Twig_Environment($loader));
        return $controller->index();
    },
];
