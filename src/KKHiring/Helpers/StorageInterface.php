<?php
namespace KKHiring\Helpers;

interface StorageInterface {
    public function connector($type);
}