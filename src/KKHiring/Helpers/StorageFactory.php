<?php

namespace KKHiring\Helpers;

use KKHiring\Libs\Database;
use KKHiring\Libs\RedisConnector;

class StorageFactory implements StorageInterface {

    public function __construct() {}

    public function connector($type)
    {
        $storageConnector = null;

        switch($type) {
            case 'mysql':
                $storageConnector = new Database();
                break;
            case 'redis':
                $storageConnector = new RedisConnector();
                break;
            default:
                $storageConnector = new RedisConnector();
                break;
        }

        return $storageConnector;
    }
}