<?php

namespace KKHiring\Services\HiringAPI;

use GuzzleHttp\Exception\RequestException;
use KKHiring\Services\HiringAPI\AuthHandler;
use KKHiring\Services\HiringAPI\StatusCodeHandling;

class HiringAPI extends ApiClient
{
    private $apiUrl;

    private $accessToken;

    private $maxAttempts = 3;

    public function __construct() {
        parent::__construct();
        $this->apiUrl = getenv('KKHIRING_API_URL');

        $authHandler = new AuthHandler();
        $this->accessToken = $authHandler->getLocalToken();
    }

    public function getEmployees()
    {
        while ($this->maxAttempts--) {
            try
            {
                $url = $this->apiUrl . '/employee/list';
                $header = array("Access-Token" => "{$this->accessToken}");
                $response = $this->client->get($url, array('headers' => $header));
                $result = $response->getBody()->getContents();
                return $result;
            } catch (RequestException $e)
            {
                $statusCodeHandler = new StatusCodeHandling();
                $statusCodeHandler->handleException($e);
            }
        }
    }
}