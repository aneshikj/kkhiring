<?php

/**
 * This is the abstract API Client for the HiringAPI which initializes just the Guzzle Client
 */

namespace KKHiring\Services\HiringAPI;

use GuzzleHttp\Client;

class ApiClient
{
    /**
     * Guzzle Instance
     **/
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }
}