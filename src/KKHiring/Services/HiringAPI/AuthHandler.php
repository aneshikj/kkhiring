<?php

/**
 * Class used for handling the Access Tokens
 */

namespace KKHiring\Services\HiringAPI;

use GuzzleHttp\Exception\RequestException;
use KKHiring\Services\HiringAPI\StatusCodeHandling;

class AuthHandler extends ApiClient {

    private $apiUrl;

    private $clientID;

    private $clientSecret;

    private $username;

    private $password;

    public function __construct() {
        parent::__construct();
        $this->apiUrl = getenv('KKHIRING_API_URL');
        $this->clientID = getenv('KKHIRING_CLIENT_ID');
        $this->clientSecret = getenv('KKHIRING_CLIENT_SECRET');
        $this->username = getenv('KKHIRING_USERNAME');
        $this->password = getenv('KKHIRING_PASSWORD');
    }

    private function getAccessToken()
    {
        try {
            $url = $this->apiUrl . '/token';

            $data = [
                'grant_type' => 'password',
                'client_id' => $this->clientID,
                'client_secret' => $this->clientSecret,
                'username' => $this->username,
                'password' => $this->password
            ];

            $response = $this->client->post($url, ['query' => $data]);
            $result = json_decode($response->getBody()->getContents());
            $this->setLocalToken($result->access_token);

            return $result->access_token;
        } catch (RequestException $e) {
            $statusCodeHandler = new StatusCodeHandling();
            $response = $statusCodeHandler->handleException($e);
            return $response;
        }
    }

    public function getLocalToken($tokenFailed = false)
    {
        if (isset($_SESSION['access_token']) && !$tokenFailed) {
            return $_SESSION['access_token'];
        }

        return $this->getAccessToken();
    }

    private function setLocalToken($accessToken)
    {
        $_SESSION['access_token'] = $accessToken;
    }
}