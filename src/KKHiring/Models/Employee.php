<?php
namespace ContactForm\Models;

use Assert\Assertion;
use Assert\AssertionFailedException;
use KKHiring\Libs\Database;

class Employee {

    /**
     * Status of the Validation
     */
    const NOT_VALID = 0;
    const VALID = 1;

    /**
     * @param string $firstName
     */
    public $firstName;

    /**
     * @param string $lastName
     */
    public $lastName;

    /**
     * @param string $email
     */
    public $email;

    /**
     * @param string $title
     */
    public $title;

    /**
     * @param string $address
     */
    public $address;

    /**
     * @param string $country
     */
    public $country;

    /**
     * @param string $bio
     */
    public $bio;

    /**
     * Connection with the Database
     *
     * @param Database $db
     */
    private $db;

    /**
     * @param array $validation
     */
    private $validation;

    public function __construct() {
        $this->db = new Database();
    }

    private function validate()
    {
        $validation = array('status' => Employee::VALID);
        try {
            Assertion::notEmpty($this->firstName, 'Name is required field');
        } catch(AssertionFailedException $e) {
            $validation['name'] = $e->getMessage();
        }

        try {
            Assertion::notEmpty($this->lastname, 'Name is required field');
        } catch(AssertionFailedException $e) {
            $validation['name'] = $e->getMessage();
        }

        try {
            Assertion::email($this->email, 'Email address is not valid');
        } catch(AssertionFailedException $e) {
            $validation['email'] = $e->getMessage();
        }

        // Check if there are validation errors
        if (count($validation) > 1) {
            $validation['status'] = Employee::NOT_VALID;
            $this->validation = $validation;
            return false;
        }

        return true;
    }

    public function save()
    {
        // If there are no validation errors save data in DB
        if ($this->validate()) {
            // Check if we're connected to the Database
            if($this->db->isConnected){
                $query = "INSERT INTO employees (firstname, lastname, email, title, bio, address, country) VALUES (?,?,?,?,?,?,?)";
                $params = array(
                    $this->firstName,
                    $this->lastName,
                    $this->email,
                    $this->title,
                    $this->bio,
                    $this->address,
                    $this->country
                    );
                $this->db->insert($query,$params);

                return array('status' => Employee::VALID);
            }
        }

        return $this->validation;
    }
}