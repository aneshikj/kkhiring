<?php

namespace KKHiring\Controllers;

use KKHiring\Helpers\StorageFactory;
use KKHiring\Services\HiringAPI\HiringAPI;
use Twig_Environment;

class EmployeesController implements ControllerInterface {

    /**
     * @var Twig_Environment $twig
     */
    private $twig;

    private $storageConnector;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
        $this->storageConnector = getenv('STORAGE_CONNECTOR');
    }

    public function index()
    {
        $storageFactory = new StorageFactory();
        $employees = $storageFactory->connector($this->storageConnector)->getEmployees();
//        print_r(json_decode($employees, true));exit;

        echo $this->twig->render('employees.twig', array('employees' => json_decode($employees, true)));
    }
}
