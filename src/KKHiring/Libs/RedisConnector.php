<?php

namespace KKHiring\Libs;
use KKHiring\Services\HiringAPI\HiringAPI;
use Predis\Client;

class RedisConnector {

    /** @var Client $predis */
    private $predis;

    private $refreshTime;

    public function __construct()
    {
        $this->predis = new Client();
        $this->refreshTime = getenv('KKHIRING_REFRESH_TIME');
    }

    public function getEmployees()
    {
        $lastUpdated = $this->predis->get('last_updated');

        if ($lastUpdated != null) {

            $employees = $this->predis->get('api_employees');

            if ($employees != null) {

                $diff = strtotime($lastUpdated) - strtotime(date('Y-m-d H:i:s'));
                $diff = $diff / (60 * 60);

                if ($diff > $this->refreshTime) {
                    return $this->getHiringAPIEmployees();
                }

                return $employees;
            }

            return $this->getHiringAPIEmployees();
        }

        return $this->getHiringAPIEmployees();
    }

    private function getHiringAPIEmployees() {
        $hiringAPI = new HiringAPI();
        $apiResults = $hiringAPI->getEmployees();

        if ($apiResults) {
            $this->predis->set('api_employees', $apiResults);
            $this->predis->set('last_updated', date('Y-m-d H:i:s'));
        }

        return $apiResults;
    }
}